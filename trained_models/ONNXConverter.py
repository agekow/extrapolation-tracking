import sys
import os

models = os.listdir()
models = []
for d in os.listdir():
    if "window" in d and os.path.isdir(d):
        models.append(d)

#for m in ['testing']:
for m in models:
    print("\n Converting Model " + m + '\n')
    os.system("python -m tf2onnx.convert --saved-model " + m + "/" + " --output onnx/" + m + ".onnx")
    print("\n Finished Converting Model " + m + "\n")
