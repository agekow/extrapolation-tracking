import tensorflow as tf
from tensorflow.keras.layers import Dense, Flatten
from tensorflow.keras.models import Sequential

def mlp(trial):
    n_layers = trial.suggest_int("n_layers", 1, 3)

    model = Sequential()
    model.add(Flatten())
    for i in range(n_layers):
        num_hidden = trial.suggest_int(f'n_units_l{i}', 8, 64, log=True)
        model.add(Dense(units=num_hidden, activation=trial.suggest_categorical("activation", ['relu', 'swish', 'gelu'])))
    model.add(Dense(3, activation='tanh'))

    learning_rate = trial.suggest_float("learning_rate", 1e-4, 1e-1, log=True)
    #learning_rate = 1e-3
    model.compile(
        loss='mse',
        optimizer=tf.keras.optimizers.Adam(learning_rate=learning_rate),
        metrics=["accuracy"]
    )

    return model