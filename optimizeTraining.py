import pickle 
import tensorflow as tf
import numpy as np
import os
import optuna
from optuna.integration import TFKerasPruningCallback
from optuna.trial import TrialState
import optModels
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"
import matplotlib.pyplot as plt
from optModels import MLP, RNN
from sklearn.model_selection import train_test_split
#from tensorflow.keras.layers import Dense, Input, BatchNormalization, Flatten, GRU
import argparse 
import tensorflow_addons as tfa

parser = argparse.ArgumentParser()
parser.add_argument("--dataDirectory", type=str, default='/eos/user/a/agekow/tracking/data/trainingPkl/withLayerInfo/', help="file name of pickle data")
parser.add_argument("--windowSize", type=int, default=3, help="window size of data")
parser.add_argument("--epochs", type=int, default=50, help="number of epochs to train for")
parser.add_argument("--batch", type=int, default=256, help="batch size")
parser.add_argument("--modelType", type=str, required=True, help="MLP, RNN, ESN, qMLP")
#parser.add_argument("--modelName", type=str, required=True, help="Name of saved model")
#parser.add_argument("--bits", type=int, default=8, help="Number of bits if using quantized network")
parser.add_argument("--local", action='store_true', help="running locally or on lxplus")
parser.add_argument("--cylindrical", action='store_true', help="use cylindrical coordinates phi/z only")
args = parser.parse_args()

if args.modelType == 'ESN': from optModels import ESN
elif args.modelType == 'qMLP': from optModels import qMLP

if args.local is True:
	gpu_devices = tf.config.experimental.list_physical_devices('GPU')
	for device in gpu_devices:
		tf.config.experimental.set_memory_growth(device, True)
    
from dataFuncs import dataWindow
if not os.path.exists("local_data/formatted/cartesian_windowSize3"):
    X = None
    Y = None
    #for data in ["Athena_ttbar_mu0_2.pkl"]:
    for data in os.listdir(args.dataDirectory)[0:1]:
        if '_R' in data: continue 
        print(data)
        with open(os.path.join(args.dataDirectory, data),'rb') as pFile:

            _ = pickle.load(pFile)
            _ = pickle.load(pFile)
            x_train = pickle.load(pFile, encoding='latin1')
            x_test = pickle.load(pFile,  encoding='latin1')
            x_train_flat = pickle.load(pFile,  encoding='latin1')
            x_test_flat = pickle.load(pFile,  encoding='latin1')
            x_train_layer = pickle.load(pFile,  encoding='latin1')
            x_test_layer = pickle.load(pFile,  encoding='latin1')
            _ = pickle.load(pFile,  encoding='latin1')
            _ =  pickle.load(pFile,  encoding='latin1')
            _ = pickle.load(pFile,  encoding='latin1')
            _ = pickle.load(pFile,  encoding='latin1')
            y_train = pickle.load(pFile,  encoding='latin1')
            y_test = pickle.load(pFile,  encoding='latin1')
        
        x_data, y_data = dataWindow(args.windowSize, x_train_flat, x_train_layer)
        x_data_test, y_data_test = dataWindow(args.windowSize, x_test_flat, x_test_layer)

        if X is None:
            X = x_data
            Y = y_data
            X = np.append(X, x_data_test, axis=0)
            Y = np.append(Y, y_data_test, axis=0)
        else:
            X = np.append(X, x_data, axis=0)
            Y = np.append(Y, y_data, axis=0)
            X = np.append(X, x_data_test, axis=0)
            Y = np.append(Y, y_data_test, axis=0)
else:
    with open("local_data/formatted/cartesian_windowSize3", "rb") as fp:
        X = pickle.load(fp)
        Y = pickle.load(fp)    

if args.cylindrical:
    xPhi = np.arctan2(X[:,:,1].flatten(), X[:,:,0].flatten()) / np.pi
    xPhi = xPhi.reshape(-1,args.windowSize,1)
    yPhi = np.arctan2(Y[:,1].flatten(), Y[:,0].flatten()) / np.pi
    yPhi = yPhi.reshape(-1,1)

    xRho = np.sqrt((X[:,:,1].flatten())**2 + (X[:,:,0].flatten())**2) 
    xRho = xRho.reshape(-1,args.windowSize,1)
    yRho = np.sqrt((Y[:,1].flatten())**2 + (Y[:,0].flatten())**2)
    yRho = yRho.reshape(-1,1)

    X = np.append(xRho, X[:,:,2].reshape(-1,args.windowSize,1), axis=2)
    del xRho
    X = np.append(X, xPhi, axis=2)
    del xPhi
    Y = np.append(yRho, Y[:,2].reshape(-1,1), axis=1)
    del yRho
    Y = np.append(Y, yPhi, axis=1)
    del yPhi


n_features=X.shape[2]
x_train, x_test, y_train, y_test = train_test_split(X,Y, test_size = .2) 
print("training input shape: ", x_train.shape)
print("training target shape: ", y_train.shape)
print("validation input shape: ", x_test.shape)
print("validation target shape: ", y_test.shape)

def objective(trial):
    tf.keras.backend.clear_session()
    model = ESN.esn(trial)
    
    model.fit(x_train, y_train, 
        batch_size = args.batch, 
        epochs=args.epochs, 
        validation_data=(x_test, y_test), 
        callbacks=[TFKerasPruningCallback(trial, "val_loss")],
        verbose=0)

    print("model trained")
    score=model.evaluate(x_test,y_test, verbose=0)
    print("score: ", score[0])
    return score[0]

study = optuna.create_study(direction="minimize", pruner=optuna.pruners.MedianPruner())
study.optimize(objective, n_trials=20)
pruned_trials = study.get_trials(deepcopy=False, states=[TrialState.PRUNED])
complete_trials = study.get_trials(deepcopy=False, states=[TrialState.COMPLETE])
print("Study statistics: ")
print("  Number of finished trials: ", len(study.trials))
print("  Number of pruned trials: ", len(pruned_trials))
print("  Number of complete trials: ", len(complete_trials))

print("Best trial:")
trial = study.best_trial

print("  Value: ", trial.value)

print("  Params: ")
for key, value in trial.params.items():
    print("    {}: {}".format(key, value))