#!/bin/bash
source /afs/cern.ch/user/a/agekow/public/myvenv_updated/bin/activate
cd /afs/cern.ch/user/a/agekow/trackml/extrapolationModels
python training_layers.py --batch 512 --modelType ESN --modelName ESN_window_size_7 --windowSize 7