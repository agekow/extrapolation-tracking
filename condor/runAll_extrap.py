import os
import shutil

#models = ['MLP', 'RNN', 'ESN']
models = ['ESN']
window_sizes = [3,  5, 7]

def main():
    for model in models:
        for window_size in window_sizes:
            modelName = "extrap" + model + '_window_size_' + str(window_size)
            dir = "extrap_" + modelName
            if os.path.exists(dir): shutil.rmtree(dir)
            os.mkdir(dir)         
            
            pyFile = 'training_layers.py --batch 512 --modelType {} --modelName {} --windowSize {}'.format(model, modelName, window_size)
            shfile = dir + '/' + dir + '.sh'
            subfile = dir + '/' + dir + '.sub'
            write_sh_file(shfile, pyFile)
            write_condor_file(shfile, subfile)
            os.system("chmod -R 775 " + dir + '/' + dir + '.sh')
            os.system('condor_submit ' + dir + '/' + dir + '.sub')


def write_sh_file(shfile, file_to_run):
    with open(shfile, "w") as shFile:
        shFile.write('#!/bin/bash\n')
        shFile.write('source /afs/cern.ch/user/a/agekow/public/myvenv_updated/bin/activate\n')
        #shFile.write('conda init bash\n')
        #shFile.write('conda activate mltrack\n')
        shFile.write('cd /afs/cern.ch/user/a/agekow/trackml/extrapolationModels/scripts\n')
        shFile.write('python ' + file_to_run)

def write_condor_file(shfile, subfile):
    s = shfile.replace(".sh", "")
    with open(subfile, "w") as subFile:
        subFile.write('executable              = ' + shfile + '\n')
        subFile.write('arguments               = $(ClusterId)$(ProcId)\n')
        subFile.write('output                  = {}.$(ClusterId).$(ProcId).out\n'.format(s))
        subFile.write('error                   = {}.$(ClusterId).$(ProcId).err\n'.format(s))
        subFile.write('log                     = {}.$(ClusterId).log\n'.format(s))
        subFile.write('request_GPUs = 1\n')
        subFile.write('request_CPUs = 2\n')
        subFile.write('+JobFlavour = workday\n')
        subFile.write('queue')


if __name__ == "__main__":
    main()
