import os
import shutil
#import glob

#models = ['MLP', 'RNN', 'ESN']
model = 'MLP'
basePath = '/eos/user/a/agekow/tracking/data/trainingPkl/v2/'
dataTypes = ['8LNominal', '8LRotatedToZero', '8LSPRotatedToZero']
def main():
    for dataType in dataTypes:
        modelName = "paramEst_" + model + '_' + dataType
        Dir = "paramEst_" + modelName
        if os.path.exists(Dir): shutil.rmtree(Dir)
        os.mkdir(Dir) 
        dataPath = basePath + '/' + dataType
        pyFile = 'training_paramEst.py --batch 512 --modelType {} --modelName {} --dataPath {}'.format(model, modelName, dataPath)
        shfile = Dir + '/' + Dir + '.sh'
        subfile = Dir + '/' + Dir + '.sub'
        write_sh_file(shfile, pyFile)
        write_condor_file(shfile, subfile)
        os.system("chmod -R 775 " + Dir + '/' + Dir + '.sh')
        os.system('condor_submit ' + Dir + '/' + Dir + '.sub')


def write_sh_file(shfile, file_to_run):
    with open(shfile, "w") as shFile:
        shFile.write('#!/bin/bash\n')
        shFile.write('source /afs/cern.ch/user/a/agekow/public/myvenv_updated/bin/activate\n')
        #shFile.write('conda init bash\n')
        #shFile.write('conda activate mltrack\n')
        shFile.write('cd /afs/cern.ch/user/a/agekow/trackml/extrapolationModels/\n')
        shFile.write('python ' + file_to_run)

def write_condor_file(shfile, subfile):
    s = shfile.replace(".sh", "")
    with open(subfile, "w") as subFile:
        subFile.write('executable              = ' + shfile + '\n')
        subFile.write('arguments               = $(ClusterId)$(ProcId)\n')
        subFile.write('output                  = {}.$(ClusterId).$(ProcId).out\n'.format(s))
        subFile.write('error                   = {}.$(ClusterId).$(ProcId).err\n'.format(s))
        subFile.write('log                     = {}.$(ClusterId).log\n'.format(s))
        subFile.write('request_GPUs = 1\n')
        subFile.write('request_CPUs = 2\n')
        subFile.write('+JobFlavour = workday\n')
        subFile.write('queue')


if __name__ == "__main__":
    main()
