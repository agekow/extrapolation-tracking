#!/bin/bash
source /afs/cern.ch/user/a/agekow/public/myvenv_updated/bin/activate
cd /afs/cern.ch/user/a/agekow/trackml/extrapolationModels
python training_layers.py --batch 512 --modelType RNN --modelName RNN_window_size_5 --windowSize 5