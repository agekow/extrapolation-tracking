import pickle 
import tensorflow as tf
import numpy as np
import os

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"
import matplotlib.pyplot as plt
from models import MLP, RNN, BidirectionalRNN, GaussianRNN
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle
#from tensorflow.keras.layers import Dense, Input, BatchNormalization, Flatten, GRU
import argparse 
import lossFunctions, learningRates
import pickle
from dataFuncs import *
import tensorflow_addons as tfa

tf.keras.backend.clear_session()

parser = argparse.ArgumentParser()
parser.add_argument("--dataDirectory", type=str, default='/eos/user/a/agekow/tracking/data/trainingPkl/withLayerInfo/', help="file name of pickle data")
parser.add_argument("--epochs", type=int, default=50, help="number of epochs to train for")
parser.add_argument("--batch", type=int, default=128, help="batch size")
parser.add_argument("--modelType", type=str, required=True, help="MLP, RNN, ESN, qMLP")
parser.add_argument("--bits", type=int, default=8, help="Number of bits if using quantized network")
parser.add_argument("--local", action='store_true', help="running locally or on lxplus")
parser.add_argument("--cylindrical", action='store_true', help="use cylindrical coordinates")
parser.add_argument("--retrain", action='store_true', help="use cylindrical coordinates")
parser.add_argument("--useLayers", action='store_true', default=False, help="use one hot encoded layer indices during training")
parser.add_argument("--barrelOnly", action='store_true', default=False, help="use only track segments in the barrel")
parser.add_argument("--endcapOnly", action='store_true', default=False, help="use only track segments in the endcap")




args = parser.parse_args()

if args.modelType == 'ESN': from models import ESN
elif args.modelType == 'qMLP': from models import qMLP

if args.local is True:
	gpu_devices = tf.config.experimental.list_physical_devices('GPU')
	for device in gpu_devices:
		tf.config.experimental.set_memory_growth(device, True)
    

# if args.barrelOnly : datafileName = 'ACTSDataWithLayerInfo_Barrel_SingleHitPerLayer'
# elif args.endcapOnly: datafileName = 'ACTSDataWithLayerInfo_EC_SingleHitPerLayer'
# else: datafileName = 'ACTSDataWithLayerInfo_SingleHitPerLayer'

with open("../novelTrackReco/novelTrackRecoBarrelData_layerInfo.pkl","rb") as f:
    X=pickle.load(f)
    Y=pickle.load(f)
    
if args.cylindrical:
    with open("../novelTrackReco/novelTrackRecoCylindricalData.pkl","rb") as f:
        X=pickle.load(f)
        Y=pickle.load(f)


#Y = Y[:,2].reshape(-1,1) # phi only


#normalize data
xNorm = 1068
yNorm = 1068
zNorm = 3025.5
X[:,:,0] = X[:,:,0]/xNorm
X[:,:,1] = X[:,:,1]/yNorm
X[:,:,2] = X[:,:,2]/zNorm
Y[:,:,0] = Y[:,:,0]/xNorm
Y[:,:,1] = Y[:,:,1]/yNorm
Y[:,:,2] = Y[:,:,2]/zNorm

Y= Y[:,:,0:3]

X = X[:, :, 0:3]
# if not args.useLayers:
#     X = X[:,:,0:3]
# Y = Y[:,:,0,2] #targets are always 3 features only x,y,z

n_inputFeatures=X.shape[2]
n_outputFeatures = Y.shape[1]

X,Y = shuffle(X,Y)
print(X[0], '\n', Y[0])
# #Try selecting a fraction of the tracks
# X = X[0:int(len(X)/8)]
# Y = Y[0:int(len(Y)/8)]
# print(X[0], Y[0])

print(Y.shape, '\n', X.shape)
loss = None
x_train, x_test, y_train, y_test = train_test_split(X,Y, test_size = .2) 

# plt.scatter(x_train[0, :, 0], X[0, :, 1])
# plt.scatter(y_train[0, :, 0], Y[0, :, 1])
# plt.show()
# if args.barrelOnly: detectorRegion = '_barrel'
# elif args.endcapOnly: detectorRegion = '_ec'
# else: detectorRegion=''
# if args.cylindrical: cyl='_cyl'
# else: cyl=''
# if args.useLayers: layers = '_layerInfo'
# else: layers=''
# modelName = 'Extrap_'+args.modelType + '_ACTS' + detectorRegion +layers + cyl
# print(modelName)
modelName = "Extrap_RNN_ACTS_barrel"
if args.retrain:
    try:
        model = tf.keras.models.load_model('trained_models/{}'.format(modelName))
        print("loaded pre-trained model")
    except: 
        print("trained_models/{} not found".format(modelName))
else:
    if args.modelType == 'RNN':
        model = RNN.rnnFullSequence(n_inputFeatures, n_outputFeatures)
    elif args.modelType == 'biRNN':
        model = BidirectionalRNN.bidrectionalRNN(args.windowSize,  n_inputFeatures, n_outputFeatures)
    elif args.modelType == 'biRNNAttention':
        model = BidirectionalRNN.bidirectionalRNNAttention(args.windowSize,  n_inputFeatures, n_outputFeatures)
    elif args.modelType == 'ESN':
        model = ESN.esn(args.windowSize,  n_inputFeatures, n_outputFeatures)
    elif args.modelType == 'gaussianRNN':
        loss = lossFunctions.nll_gaussian
        model = GaussianRNN.gaussianRNN(args.windowSize,  n_inputFeatures, n_outputFeatures)
    elif args.modelType == 'choleskyRNN':
        model = GaussianRNN.choleskyRNN(args.windowSize,  n_inputFeatures, n_outputFeatures)
        loss = lossFunctions.nll_gaussian_cholensky

    #elif args.modelType == 'qMLP':
    #    model = qMLP.qmlp(window_size=args.windowSize, n_features=n_features, bits=args.bits)

    
checkpoint_filepath = 'checkpoints/' + modelName
if os.path.exists(checkpoint_filepath): os.rmdir(checkpoint_filepath)
os.mkdir(checkpoint_filepath)
callbacks = [tf.keras.callbacks.ModelCheckpoint(checkpoint_filepath, monitor='val_loss', verbose=0, save_best_only=True, save_weights_only=True),
			tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=50)]


#from learningRates import transformerLR
warmup_steps = x_train.shape[0] // args.batch * args.epochs // 5 # 1/5 of the training will be warmup steps
learning_rate = learningRates.CustomSchedule(64, warmup_steps)
#learning_rate = learningRates.OCP(warmup_steps=warmup_steps)
#learning_rate=1e-2
#lossFunc = lossFunctions.mseNorm
optimizer = tf.keras.optimizers.Adam(learning_rate, beta_1=0.9, beta_2=0.98, epsilon=1e-9)
if loss is None: loss='mse'
#optimizer = learningRates.CLR(1e-4, 5e-2, x_train.shape[0] // args.batch*2)
model.compile(optimizer=optimizer,  loss=loss)
history = model.fit(x_train, y_train, epochs=args.epochs, batch_size=args.batch, validation_data=(x_test, y_test), callbacks=callbacks)
model.load_weights(checkpoint_filepath)


#Hard negative mining

# pred = model.predict(x_train)
# mse = tf.reduce_mean(tf.math.square(y_train-pred), axis=1) #mse per input
# del pred


# errorIdx = np.where(mse>1e-4)[0]
# model.fit(x_train[errorIdx], y_train[errorIdx], 
#     epochs=50, batch_size=args.batch, 
#     validation_data=([x_test, y_test]),
#     callbacks=callbacks)
# model.load_weights(checkpoint_filepath)

from shutil import rmtree
modelPath = "trained_models/" +modelName
if os.path.exists(modelPath): rmtree(modelPath)
tf.keras.models.save_model(model, modelPath, include_optimizer=False)
plt.plot(range(len(history.history['loss'])), history.history['loss'], label='train')
plt.plot(range(len(history.history['val_loss'])), history.history['val_loss'], label='train')
plt.xlabel('epochs')
plt.ylabel('loss')
plt.savefig('trained_models/{}_loss'.format(modelName))

print(modelName)
