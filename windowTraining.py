import pickle 
import tensorflow as tf
import numpy as np
import os

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"
import matplotlib.pyplot as plt
from models import MLP, RNN, BidirectionalRNN, GaussianRNN
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle
#from tensorflow.keras.layers import Dense, Input, BatchNormalization, Flatten, GRU
import argparse 
import lossFunctions, learningRates
import pickle
from dataFuncs import *
import tensorflow_addons as tfa

tf.keras.backend.clear_session()

parser = argparse.ArgumentParser()
parser.add_argument("--dataDirectory", type=str, default='/eos/user/a/agekow/tracking/data/trainingPkl/withLayerInfo/', help="file name of pickle data")
parser.add_argument("--windowSize", type=int, default=3, help="window size of data")
parser.add_argument("--epochs", type=int, default=50, help="number of epochs to train for")
parser.add_argument("--batch", type=int, default=128, help="batch size")
parser.add_argument("--modelType", type=str, required=True, help="MLP, RNN, ESN, qMLP")
#parser.add_argument("--modelName", type=str, required=True, help="Name of saved model")
parser.add_argument("--bits", type=int, default=8, help="Number of bits if using quantized network")
parser.add_argument("--local", action='store_true', help="running locally or on lxplus")
parser.add_argument("--cylindrical", action='store_true', help="use cylindrical coordinates")
parser.add_argument("--retrain", action='store_true', help="use cylindrical coordinates")
parser.add_argument("--useLayers", action='store_true', default=False, help="use one hot encoded layer indices during training")
parser.add_argument("--barrelOnly", action='store_true', default=False, help="use only track segments in the barrel")
parser.add_argument("--endcapOnly", action='store_true', default=False, help="use only track segments in the endcap")




args = parser.parse_args()

if args.modelType == 'ESN': from models import ESN
elif args.modelType == 'qMLP': from models import qMLP

if args.local is True:
	gpu_devices = tf.config.experimental.list_physical_devices('GPU')
	for device in gpu_devices:
		tf.config.experimental.set_memory_growth(device, True)
    

if args.barrelOnly : datafileName = 'ACTSDataWithLayerInfo_Barrel_SingleHitPerLayer'
elif args.endcapOnly: datafileName = 'ACTSDataWithLayerInfo_EC_SingleHitPerLayer'
else: datafileName = 'ACTSDataWithLayerInfo_SingleHitPerLayer'
datafileName = datafileName + '_windowSize{}'.format(args.windowSize)
print(datafileName)
if not os.path.exists("local_data/formatted/{}".format(datafileName)):
    X = None
    Y = None
    #for data in ["Athena_ttbar_mu0_2.pkl"]:
    for data in os.listdir(args.dataDirectory):
        if not 'ACTS' in data: continue 
        if '200' in data: continue
        print("starting", data)
        with open(os.path.join(args.dataDirectory, data),'rb') as pFile:

            _ = pickle.load(pFile)
            _ = pickle.load(pFile)
            x_train = pickle.load(pFile, encoding='latin1')
            x_test = pickle.load(pFile,  encoding='latin1')
            x_train_flat = pickle.load(pFile,  encoding='latin1')
            x_test_flat = pickle.load(pFile,  encoding='latin1')
            x_train_layer = pickle.load(pFile,  encoding='latin1')
            x_test_layer = pickle.load(pFile,  encoding='latin1')
            _ = pickle.load(pFile,  encoding='latin1')
            _ =  pickle.load(pFile,  encoding='latin1')
            _ = pickle.load(pFile,  encoding='latin1')
            _ = pickle.load(pFile,  encoding='latin1')
            y_train = pickle.load(pFile,  encoding='latin1')
            y_test = pickle.load(pFile,  encoding='latin1')
        
        x_data, y_data = dataWindowACTS(args.windowSize, x_train_flat, x_train_layer, 12, args.barrelOnly, args.endcapOnly)
        x_data_test, y_data_test = dataWindowACTS(args.windowSize, x_test_flat, x_test_layer, 12, args.barrelOnly, args.endcapOnly)

        if X is None:
            X = x_data
            Y = y_data
            X = np.append(X, x_data_test, axis=0)
            Y = np.append(Y, y_data_test, axis=0)
        else:
            X = np.append(X, x_data, axis=0)
            Y = np.append(Y, y_data, axis=0)
            X = np.append(X, x_data_test, axis=0)
            Y = np.append(Y, y_data_test, axis=0)
        print("finished", data)
    with open("local_data/formatted/{}".format(datafileName), "wb") as fp:
        pickle.dump(X, fp)
        pickle.dump(Y, fp)
        
else:
    with open("local_data/formatted/{}".format(datafileName), "rb") as fp:
        X = pickle.load(fp)
        Y = pickle.load(fp)    

if args.cylindrical:
    xPhi = np.arctan2(X[:,:,1].flatten(), X[:,:,0].flatten()) / np.pi
    xPhi = xPhi.reshape(-1,3,1)
    yPhi = np.arctan2(Y[:,1].flatten(), Y[:,0].flatten()) / np.pi
    yPhi = yPhi.reshape(-1,1)

    xRho = np.sqrt((X[:,:,1].flatten())**2 + (X[:,:,0].flatten())**2 ) / 1068
    xRho = xRho.reshape(-1,3,1)
    yRho = np.sqrt((Y[:,1].flatten())**2 + (Y[:,0].flatten())**2+(Y[:,2].flatten())**2) / 1068
    yRho = yRho.reshape(-1,1)

    #(rho, z, phi)
    X = np.append(xRho, X[:,:,2].reshape(-1,3,1) / 3025.5, axis=2)
    del xRho
    X = np.append(X, xPhi, axis=2)
    del xPhi
    Y = np.append(yRho, Y[:,2].reshape(-1,1) / 3025.5, axis=1)
    del yRho
    Y = np.append(Y, yPhi, axis=1)
    del yPhi

    cyl = '_cylindrical'
else:
    cyl = ''


#Y = Y[:,1:] # phi/Z only


#normalize data
if not args.cylindrical:
    xNorm = 1068
    yNorm = 1068
    zNorm = 3025.5
    X[:,:,0] = X[:,:,0]/xNorm
    X[:,:,1] = X[:,:,1]/yNorm
    X[:,:,2] = X[:,:,2]/zNorm
    Y[:,0] = Y[:,0]/xNorm
    Y[:,1] = Y[:,1]/yNorm
    Y[:,2] = Y[:,2]/zNorm

if not args.useLayers:
    X = X[:,:,0:3]
Y = Y[:,0:3] #targets are always 3 features only x,y,z
Y = Y[:,1:3] #targets are 2 features only z/phi

n_inputFeatures=X.shape[2]
n_outputFeatures = Y.shape[1]

X,Y = shuffle(X,Y)

# #Try selecting a fraction of the tracks
# X = X[0:int(len(X)/8)]
# Y = Y[0:int(len(Y)/8)]
# print(X[0], Y[0])

print(Y.shape, '\n', X.shape)
loss = None
x_train, x_test, y_train, y_test = train_test_split(X,Y, test_size = .2) 

# plt.scatter(x_train[0, :, 0], X[0, :, 1])
# plt.scatter(y_train[0, :, 0], Y[0, :, 1])
# plt.show()
if args.barrelOnly: detectorRegion = '_barrel'
elif args.endcapOnly: detectorRegion = '_ec'
else: detectorRegion=''

if args.useLayers: layers = '_layerInfo'
else: layers=''
modelName = 'Extrap_'+args.modelType + '_ACTS_ws' + str(args.windowSize) + cyl+ detectorRegion +layers
print(modelName)
if args.retrain:
    model = tf.keras.models.load_model('trained_models/{}'.format(modelName))
    print("loaded pre-trained model")


else:
    if args.modelType == 'RNN':
        model = RNN.rnn(args.windowSize, n_inputFeatures, n_outputFeatures)
    elif args.modelType == 'biRNN':
        model = BidirectionalRNN.bidrectionalRNN(args.windowSize,  n_inputFeatures, n_outputFeatures)
    elif args.modelType == 'biRNNAttention':
        model = BidirectionalRNN.bidirectionalRNNAttention(args.windowSize,  n_inputFeatures, n_outputFeatures)
    elif args.modelType == 'MLP':
        model = MLP.mlp(args.windowSize, n_outputFeatures)
    elif args.modelType == 'ESN':
        model = ESN.esn(args.windowSize, n_inputFeatures, n_outputFeatures)
    elif args.modelType == 'gaussianRNN':
        loss = lossFunctions.nll_gaussian
        model = GaussianRNN.gaussianRNN(args.windowSize,  n_inputFeatures, n_outputFeatures)
    elif args.modelType == 'choleskyRNN':
           model = GaussianRNN.choleskyRNN(args.windowSize,  n_inputFeatures, n_outputFeatures)
           loss = lossFunctions.nll_gaussian_cholensky

    #elif args.modelType == 'qMLP':
    #    model = qMLP.qmlp(window_size=args.windowSize, n_features=n_features, bits=args.bits)
    
    
checkpoint_filepath = 'checkpoints/' + modelName
if os.path.exists(checkpoint_filepath): os.rmdir(checkpoint_filepath)
os.mkdir(checkpoint_filepath)
callbacks = [tf.keras.callbacks.ModelCheckpoint(checkpoint_filepath, monitor='val_loss', verbose=0, save_best_only=True, save_weights_only=True),
			tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=50)]


#from learningRates import transformerLR
warmup_steps = x_train.shape[0] // args.batch * args.epochs // 5 # 1/5 of the training will be warmup steps
learning_rate = learningRates.CustomSchedule(64, warmup_steps)
#learning_rate = learningRates.OCP(warmup_steps=warmup_steps)
#learning_rate=1e-2
#lossFunc = lossFunctions.mseNorm
optimizer = tf.keras.optimizers.Adam(learning_rate, beta_1=0.9, beta_2=0.98, epsilon=1e-9)
#if loss is None: loss='mse'
#loss = lossFunctions.penalizeRho
loss = 'mse'
#optimizer = learningRates.CLR(1e-4, 5e-2, x_train.shape[0] // args.batch*2)
model.compile(optimizer=optimizer,  loss=loss)
history = model.fit(x_train, y_train, epochs=args.epochs, batch_size=args.batch, validation_data=(x_test, y_test), callbacks=callbacks)
model.load_weights(checkpoint_filepath)

#Hard negative mining

# pred = model.predict(x_train)
# mse = tf.reduce_mean(tf.math.square(y_train-pred), axis=1) #mse per input
# del pred

# errorIdx = np.where(mse>1e-4)[0]
# model.fit(x_train[errorIdx], y_train[errorIdx], 
#     epochs=50, batch_size=args.batch, 
#     validation_data=([x_test, y_test]),
#     callbacks=callbacks)
# model.load_weights(checkpoint_filepath)

from shutil import rmtree
modelPath = "trained_models/" +modelName
if os.path.exists(modelPath): rmtree(modelPath)
tf.keras.models.save_model(model, modelPath, include_optimizer=False)
plt.plot(range(len(history.history['loss'])), history.history['loss'], label='train')
plt.plot(range(len(history.history['val_loss'])), history.history['val_loss'], label='train')
plt.xlabel('epochs')
plt.ylabel('loss')
plt.savefig('trained_models/{}_loss'.format(modelName))

print(modelName)
