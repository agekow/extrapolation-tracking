
import pickle 
import tensorflow as tf
import tensorflow_addons as tfa
import numpy as np
from models import ESN, RNN, MLP
import os
import matplotlib.pyplot as plt
import argparse

parser = argparse.ArgumentParser()
#parser.add_argument("--dataDirectory", type=str, default='', help="file name of pickle data")
parser.add_argument("--windowSize", type=int, default=3, help="window size of data")
parser.add_argument("--model", type=str, default = 'MLP_window_size_3', help='name of saved model')
args = parser.parse_args()
def dataWindow(window_size, tracks, layer_info, n_features):
    #Input (tracks) should be an array of shape (n_samples, n_hits*n_features where the features are listed as x0, y0, z0, x1, y2, z2, ...)
    # returns an x array of size (n_samples, window_size, n_features)
    # returns a y array of size (n_samples, n_features)
    seeds = {}
    targets = {}
    track_list = []
    trackSeed_idx = []
    track_idx = 0 
    #for i in range(1):
    for i in range(tracks.shape[0]):
        detector_layers = layer_info.iloc[i][15:]
        track = tracks[i]
        x=track[0:15]
        y=track[15:30]
        z=track[30:]
        new_track = None
        prev_layer_id = -999
        for j in range(15):
            layer_id = detector_layers[j]
            if layer_id != prev_layer_id:
                hit = np.array([x[j], y[j], z[j]])
                if np.any(hit) == 0: break

                if np.any(new_track) is None: 
                    new_track = hit
                else: 
                    new_track = np.vstack([new_track,hit])
            else:
                continue
            prev_layer_id = layer_id
            
        if len(new_track) > 4:
            track_list.append(track)
        else:
            continue
        
        seeds[i] = None
        targets[i] = None
        n_hits = new_track.shape[0]
        
        for w in range(n_hits - window_size):
            seed = new_track[w:w+window_size]
            target = new_track[w+window_size]
            
            if seeds[i] is None:
                seeds[i] = seed
                targets[i] = target
            else:
                seeds[i] = np.append(seeds[i], seed, axis=0)
                targets[i] = np.append(targets[i], target, axis=0)
                
            trackSeed_idx.append(track_idx)
        track_idx += 1
    
    x = None
    y = None
    for key in seeds.keys():
        if x is None:
            x = seeds[key]
            y = targets[key]
        else:
            x = np.append(x, seeds[key])
            y = np.append(y, targets[key])


    return x.reshape(-1,3,3), y.reshape(-1,3), track_list, trackSeed_idx

def displacement(true_hit, pred_hit):
    
    x_true = true_hit[0]
    y_true = true_hit[1]
    z_true = true_hit[2]
    
    x_pred = pred_hit[0]
    y_pred = pred_hit[1]
    z_pred = pred_hit[2]
    
    return np.sqrt((x_true - x_pred)**2 + (y_true-y_pred)**2 + (z_true-z_pred)**2)

def unScale(hits):
    
    if len(hits.shape) == 2:
        hits[:,0] = hits[:, 0]*1015
        hits[:,1] = hits[:, 1]*1015
        hits[:,2] = hits[:, 2]*3000
    elif len(hits.shape) == 1:
        hits[0] = hits[0]*1015
        hits[1] = hits[1]*1015
        hits[2] = hits[2]*3000
    else:
        print('Input shape is not valid')

    return hits

def plotTrackErrors(track, seed, target, predicted, filename):
    #track is in the form x0, x1....x14, y0, y1, ....

    seed = unScale(seed)
    #target = unScale(target)
    #predicted = unScale(predictdfi)
    trkX = track[0:15]*1015
    trkY = track[15:30]*1015
    trkZ = track[30:]*3000
    
    
    trkRho = np.sqrt(trkX**2 + trkY**2)
    seedRho = np.sqrt(seed[:, 0]**2 + seed[:, 1]**2)
    targetRho = np.sqrt(target[0]**2 + target[1]**2)
    predictedRho = np.sqrt(predicted[0]**2 + predicted[1]**2)
    
    fig, ax = plt.subplots(1, 2, figsize=(12,6))
    ax[0].scatter(trkX , trkY, color='blue', label='track')
    ax[0].scatter(seed[:, 0], seed[:, 1], color='purple', label='seed')
    ax[0].scatter(target[0], target[1], color='red', label='target')
    ax[0].scatter(predicted[0], predicted[1], color='green', marker='*', label='predicted')
    ax[0].legend()
    ax[0].set_xlabel('X')
    ax[0].set_ylabel('Y')
    
    #fig, ax = plt.subplots(1, 2, figsize=(12,6))
    ax[1].scatter(trkZ , trkRho, color='blue', label='track')
    ax[1].scatter(seed[:, 2], seedRho, color='purple', label='seed')
    ax[1].scatter(target[2], targetRho, color='red', label='target')
    ax[1].scatter(predicted[2], predictedRho, color='green', marker='*', label='predicted')
    ax[1].legend()
    ax[1].set_xlabel('Z')
    ax[1].set_ylabel(r'$\rho$')
    
    plt.savefig('errorAnalysis/'+ filename)
    plt.close()

#filepath = '/eos/user/a/agekow/tracking/data/trainingPkl/withLayerInfo/'
filepath = 'local_data/extrapData/'
for f in os.listdir(filepath):
    with open(filepath+f, 'rb') as pFile:
            _ = pickle.load(pFile, encoding='latin1')
            _ = pickle.load(pFile, encoding='latin1')

            x_train = pickle.load(pFile, encoding='latin1')
            x_test = pickle.load(pFile,  encoding='latin1')
            x_train_flat = pickle.load(pFile,  encoding='latin1')
            x_test_flat = pickle.load(pFile,  encoding='latin1')
            x_train_layer = pickle.load(pFile,  encoding='latin1')
            x_test_layer = pickle.load(pFile,  encoding='latin1')

            _ = pickle.load(pFile,  encoding='latin1')
            _ =  pickle.load(pFile,  encoding='latin1')
            _ = pickle.load(pFile,  encoding='latin1')
            _ = pickle.load(pFile,  encoding='latin1')

            y_train = pickle.load(pFile,  encoding='latin1')
            y_test = pickle.load(pFile,  encoding='latin1')




    

    try: x_test,y_test, track_list, trackSeed_idx = dataWindow(args.windowSize, x_train_flat, x_train_layer, 3)
    except: continue

    model = tf.keras.models.load_model('trained_models/'+args.model, compile=False)

    # plot displacement error as function of z, rho
    predictions = model.predict(x_test[:, :, 0:3])
    predictions = unScale(predictions)

    y_test = unScale(y_test)    
    err = []
    for i in range(len(predictions)):
        err.append(displacement(y_test[i], predictions[i]))
    err = np.array(err)

    # Histogram the error distribution
    plt.hist(err, bins=100, histtype='step')
    plt.xlabel('prediction error (mm)')
    plt.savefig('errorAnalysis/errorHist_{}.pdf'.format(f))
    plt.close()

    #Histogram the distribution of predicted rho values
    plt.hist(np.sqrt(predictions[:,0]**2 + predictions[:,1]**2), bins=100, histtype='step', label='predicted')
    plt.hist(np.sqrt(y_test[:,0]**2 + y_test[:,1]**2), bins=100, histtype='step', label='truth')
    plt.xlabel(r'$\rho$ (mm)')
    plt.legend()
    plt.savefig('errorAnalysis/rhoDistribution_{}.pdf'.format(f))
    plt.close()

    #Histogram the distribution of predicted z values
    plt.hist(predictions[:,2], bins=100, histtype='step', label='predicted')
    plt.hist(y_test[:,2], bins=100, histtype='step', label='truth')
    plt.xlabel('Z (mm)')
    plt.legend()
    plt.savefig('errorAnalysis/zDistribution_{}.pdf'.format(f))
    plt.close()


    ## Find where error is very large, plot these tracks and predictions
    seed_loc = np.where(err > 200)[0]
    good_seed_loc = np.where(err < 10)[0]

for i in range(0, 40, 5):
    idx = seed_loc[i]
    track_idx = trackSeed_idx[idx]
    good_idx = good_seed_loc[i]
    good_track_idx = trackSeed_idx[good_idx]	
    plotTrackErrors(track=track_list[track_idx], seed=x_test[idx], target=y_test[idx], predicted=predictions[idx], filename='bad_track_{}_{}.pdf'.format(f,i))
    plotTrackErrors(track=track_list[good_track_idx], seed=x_test[good_idx], target=y_test[good_idx], predicted=predictions[good_idx], filename='good_track_{}_{}.pdf'.format(f,i))
