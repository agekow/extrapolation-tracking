import tensorflow as tf
from tensorflow.keras.layers import Input, GRU, Dense

class Sampling(tf.keras.layers.Layer):
    def call(self, inputs):
        mu, log_var = inputs
        batch = tf.shape(mu)[0]
        dim = tf.shape(mu)[1]
        eps = tf.keras.backend.random_normal(shape=(batch,dim))
        return mu + tf.exp(0.5 * log_var) * eps


class choleskyRNN(tf.keras.Model):
    def __init__(self, window_size=3, n_inputFeatures=3, n_outputFeatures=3):
        super().__init__()

        #self.sampling = Sampling()
        #self.inputs = Input(shape=(window_size,n_inputFeatures))
        self.gru1 = GRU(32, return_sequences=True)
        self.gru2 = GRU(32, return_sequences=False)
        self.mean = Dense(n_outputFeatures, activation='tanh')
        #Interpret the output of var as the elements in the cholensky decomp of the covariance matrix.
        # Alternatively, interpret it as the log(var)
        #self.var = Dense(n_outputFeatures)
        self.var = Dense(n_outputFeatures+3, activation='elu') #elu activation forces the covariance elements to be positive
        self.concat = tf.keras.layers.Concatenate()
        
    def call(self, inputs):
        #x = self.inputs()
        x = self.gru1(inputs)
        x = self.gru2(x)
        mean = self.mean(x)
        var = self.var(x)
        #sample = self.sampling([mean, var])
        #outputs = self.concat([sample, mean, var]) 
        outputs = self.concat([mean, var]) 
        return outputs

    def train_step(self, inputs):
        inputs, targets = inputs
        with tf.GradientTape() as tape:
            out = self(inputs)
            mean = out[0:3]
            #log_var = out[3:6]
            sample = out[6:]
            loss = self.compiled_loss(targets, sample)

        #should this be indented?
        grads = tape.gradient(loss, self.trainable_weights)
        self.optimizer.apply_gradients(zip(grads, self.trainable_weights))
        return {m.name: m.result() for m in self.metrics}

def gaussianRNN(window_size=3, n_inputFeatures=2, n_outputFeatures=2):
    inputs = Input(shape=(window_size,n_inputFeatures))
    x = GRU(64, return_sequences=True)(inputs)
    x = GRU(32, return_sequences=False)(x)
    mu = Dense(3, activation='tanh', name='mu')(x)
    log_var = Dense(n_outputFeatures, activation='linear', name='log_var')(x)
    #sampling necessary?
    outputs = tf.keras.layers.Concatenate()([mu, log_var]) 
    model = tf.keras.Model(inputs=inputs, outputs=outputs)
    return model