import tensorflow as tf
from tensorflow.keras.layers import Dense, Flatten
from tensorflow.keras.models import Sequential

def mlp(window_size=3, n_features=3):
    #window_size refers to the number of hits included in a data sample used for prediction
    #n_feautres refers to the number of features per hit, ex. 3 for x,y,z
    model  = Sequential([
        Flatten(), 
        Dense(64, activation='relu'),
        Dense(32, activation='relu'),
        Dense(n_features, activation='tanh')
    ])
    return model