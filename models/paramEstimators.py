import tensorflow as tf
from tensorflow.keras.layers import Dense, Flatten, BatchNormalization, Masking, GRU
import tensorflow_addons as tfa

def paramEstimator_mlp(nHits, n_features=3):
    #window_size refers to the number of hits included in a data sample used for prediction
    #n_feautres refers to the number of features per hit, ex. 3 for x,y,z
    
    input_layer = tf.keras.Input(shape=(nHits))
    #input_layer = tf.keras.Input(shape=(nHits, n_features))
    #flatten = Flatten()(input_layer)
    dense_1 = Dense(64, activation='relu')(input_layer)
    batch_norm = BatchNormalization()(dense_1)
    dense_2 = Dense(64, activation='relu')(batch_norm)
    output = Dense(3)(dense_2) #[pt, d0, z0]
    # #break output into 3 for easier labeling
    # d0 = Dense(1)(dense_2)
    # z0 = Dense(1)(dense_2)
    # pt = Dense(1)(dense_2)

    model = tf.keras.models.Model(inputs=input_layer, outputs=output)

    return model

def paramEstimator_gru(nHits, n_features=3):
    inputs = tf.keras.Input(shape=(nHits,n_features))
    x = Masking(mask_value=0)(inputs)
    x = GRU(32, return_sequences=True)(x)
    x = GRU(32, return_sequences=False)(x)
    outputs = Dense(3)(x)

    model = tf.keras.Model(inputs=inputs, outputs=outputs)
    return model

def paramEstimator_esn(nHits, n_features=3):
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.InputLayer(input_shape=(nHits,n_features)))
    model.add(tfa.layers.ESN(100))
    model.add(tf.keras.layers.Dense(3))
    return model