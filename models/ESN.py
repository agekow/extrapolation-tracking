from grpc import ChannelConnectivity
import tensorflow as tf
import tensorflow_addons as tfa

def esn(window_size=3, n_inputFeatures=3, n_outputFeatures=3):
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.InputLayer(input_shape=(window_size,n_inputFeatures)))
    model.add(tfa.layers.ESN(100, connectivity=.5))
    #model.add(tf.keras.layers.Dense(32, activation='relu'))
    #model.add(tf.keras.layers.Dense(32, activation='relu'))
    model.add(tf.keras.layers.Dense(n_outputFeatures, activation='tanh'))
    return model
