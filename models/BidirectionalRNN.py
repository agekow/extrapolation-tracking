import tensorflow as tf
from tensorflow.keras.layers import Dense, GRU, Input, Bidirectional, Attention
import tensorflow_addons as tfa

def bidrectionalRNN(window_size=3, n_inputFeatures=20,n_outputFeatures=3):
    inputs = Input(shape=(window_size,n_inputFeatures))
    #x = Masking(mask_value=0)(inputs)
    x = Bidirectional(GRU(32, return_sequences=True))(inputs)
    #x = LSTM(32, return_sequences=True)(x)
    #x = LayerNormalization()(x)
    x = Bidirectional(GRU(32, return_sequences=False))(x)
    #x = LayerNormalization()(x)
    x = Dense(64, activation='relu')(x)
    outputs = Dense(n_outputFeatures, activation='tanh', name='mu')(x)
    #outputs = Dense(3, activation='tanh', name='mu')(x)
    model = tf.keras.Model(inputs=inputs, outputs=outputs)
    return model

# Need to use self attention, not encoder-decoder attention
def bidirectionalRNNAttention(window_size=3, n_inputFeatures=20,n_outputFeatures=3):
    inputs = Input(shape=(window_size,n_inputFeatures))
    x = Bidirectional(GRU(32, return_sequences=True))(inputs)
    x = Attention()([x, x])
    x = Bidirectional(GRU(32, return_sequences=False))(x)
    outputs = Dense(n_outputFeatures, activation='tanh')(x)
    model = tf.keras.Model(inputs=inputs, outputs=outputs)
    return model






