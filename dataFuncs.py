import numpy as np

def dataWindow(window_size, tracks, layer_info, n_features=3):
    #Input (tracks) should be an array of shape (n_samples, n_hits*n_features where the features are listed as x0, y0, z0, x1, y2, z2, ...)
    # returns an x array of size (n_samples, window_size, n_features)
    # returns a y array of size (n_samples, n_features)
    seeds = {}
    targets = {}
   
    #for i in range(1):
    for i in range(tracks.shape[0]):
        
        detector_layers = layer_info.iloc[i][15:]
        detector_volumes = layer_info.iloc[i][0:15]
        track = tracks[i]
        x=track[0:15]
        y=track[15:30]
        z=track[30:]
        new_track = None
        prev_layer_id = -999
        prev_volume_id = -999
        for j in range(15):
            layer_id = detector_layers[j]
            volume_id = detector_volumes[j]
            
            # if the hit is in the same volume as the previous hit, ensure it is in the next consecutive layer
            if volume_id == prev_volume_id:
                if layer_id == prev_layer_id +1:
                    hit = np.array([x[j], y[j], z[j]])
                    if np.any(hit) == 0: break
                    prev_layer_id = layer_id
                    if np.any(new_track) is None: 
                        new_track = hit
                    else: 
                        new_track = np.vstack([new_track,hit])
                else: continue
                    
            else: 
                hit = np.array([x[j], y[j], z[j]])
                if np.any(hit) == 0: break
                prev_layer_id = layer_id
                prev_volume_id = volume_id
                if np.any(new_track) is None: 
                    new_track = hit
                else: 
                    new_track = np.vstack([new_track,hit])

        if len(new_track) < window_size+1: continue
        
        seeds[i] = None
        targets[i] = None
        n_hits = new_track.shape[0]
        
        for w in range(n_hits - window_size):
            seed = new_track[w:w+window_size]
            target = new_track[w+window_size]
            
            if seeds[i] is None:
                seeds[i] = seed
                targets[i] = target
            else:
                seeds[i] = np.append(seeds[i], seed, axis=0)
                targets[i] = np.append(targets[i], target, axis=0)

        
    
    x = None
    y = None
    for key in seeds.keys():
        if x is None:
            x = seeds[key]
            y = targets[key]
        else:
            x = np.append(x, seeds[key])
            y = np.append(y, targets[key])

    
    return x.reshape(-1,window_size,3), y.reshape(-1,3)


def detectorInfo_to_featureVector(detectorInfoLoc, cols, nhits):
    track = []
    for i in range(nhits):
        zeros = np.zeros((9,)) # first spot is 0/1 for ec/barrel. Next several are One-hot-encoded layer id
        vol_id = detectorInfoLoc[cols[i]]
        layer_idx = detectorInfoLoc[cols[i+15]]
        zeros[0] = vol_id
        if layer_idx !=0: zeros[int(layer_idx/2)] = 1 #layer id are even only, convert to consecutive integers
        track.append(zeros)
    return np.asarray(track)

def dataWindowACTS(window_size, tracks, detectorInfo, n_features=3, barrelOnly=False, endcapOnly=False):
    #Input (tracks) should be an array of shape (n_samples, n_hits*n_features where the features are listed as x0, y0, z0, x1, y2, z2, ...)
    # returns an x array of size (n_samples, window_size, n_features)
    # returns a y array of size (n_samples, n_features)
    seeds = []
    targets = []
    cols = detectorInfo.columns
    #for i in range(1):
    for i in range(tracks.shape[0]):

        track = tracks[i]
        x=track[0:15]
        y=track[15:30]
        z=track[30:]
        detectorID = detectorInfo.iloc[i]
        new_track = None
        
        nHits = np.where(track==0)[0] 
        if not np.any(nHits): nHits = 15
        else: nHits = nHits[0]
        
        # use only a single hit per layer
        detectorFeatures = detectorInfo_to_featureVector(detectorID, cols, nHits)
        detector = np.array([])
        indices = []
        for j in range(nHits):
            currentDetector = detectorFeatures[j]
            if np.array_equal(detector, currentDetector): continue
            detector = currentDetector
            indices.append(j)
        nHits = len(indices)
        new_track = np.append(x[indices].reshape(nHits,1), y[indices].reshape(nHits,1),axis=1)
        new_track = np.append(new_track, z[indices].reshape(nHits,1), axis=1)
        new_track = np.append(new_track, detectorFeatures[indices], axis=1)

        for w in range(nHits - window_size):

            seed = new_track[w:w+window_size].reshape(1,window_size,n_features)
            target = new_track[w+window_size].reshape(n_features)
            if (barrelOnly and ((np.any(seed[:,:,3] == 1)) or (np.any(target[3]==1)))): break
            if (endcapOnly and ((np.any(seed[:,:,3] == 0)) or (np.any(target[3]==0)))): break
            seeds.append(seed)
            targets.append(target)
        if i%10000 == 0: print("completed", i, "tracks")


    seeds = np.asarray(seeds)
    targets = np.asarray(targets)
    
    return seeds.reshape(-1,window_size,n_features), targets.reshape(-1,n_features)

def tracksACTS(tracks, detectorInfo, n_features=3, barrelOnly=False, endcapOnly=False):
    #Input (tracks) should be an array of shape (n_samples, n_hits*n_features where the features are listed as x0, y0, z0, x1, y2, z2, ...)
    # returns an x array of size (n_samples, window_size, n_features)
    # returns a y array of size (n_samples, n_features)
    completeTracks = []
    cols = detectorInfo.columns
    #for i in range(2):
    for i in range(tracks.shape[0]):

        track = tracks[i]
        x=track[0:15]
        y=track[15:30]
        z=track[30:]
        detectorID = detectorInfo.iloc[i]
        new_track = None
        
        nHits = np.where(track==0)[0] 
        if not np.any(nHits): nHits = 15
        else: nHits = nHits[0]
        
        # use only a single hit per layer
        detectorFeatures = detectorInfo_to_featureVector(detectorID, cols, nHits)
        detector = np.array([])
        indices = []
        for j in range(nHits):
            currentDetector = detectorFeatures[j]
            if np.array_equal(detector, currentDetector): continue
            detector = currentDetector
            indices.append(j)
        nHits = len(indices)
        new_track = np.append(x[indices].reshape(nHits,1), y[indices].reshape(nHits,1),axis=1)
        new_track = np.append(new_track, z[indices].reshape(nHits,1), axis=1)
        new_track = np.append(new_track, detectorFeatures[indices], axis=1)
        #new_track = new_track.reshape(1, len(new_track), n_features)
        
        if (barrelOnly):
            idx = np.where(new_track[:,3]==1)[0] #find where the endcap hits begin
            if len(idx) > 0: new_track = new_track[:idx[0]] #select up to the endcap hits
                
        if len(new_track) < 15:
            zeros = np.zeros((15-len(new_track), n_features))
            #zeros = zeros[np.newaxis,:,:]
            new_track = np.append(new_track, zeros, axis=0)

        
        
        
        completeTracks.append(new_track)
        if i%10000 == 0: print("completed", i, "tracks")
    completeTracks = np.asarray(completeTracks)
    inputs = completeTracks[:, :-1, :]
    targets = completeTracks[:, 1:, :]
    
    return inputs, targets