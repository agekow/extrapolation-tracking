import pickle 
import tensorflow as tf
import numpy as np
import os
import matplotlib.pyplot as plt
from models import paramEstimators
from sklearn.model_selection import train_test_split
#from tensorflow.keras.layers import Dense, Input, BatchNormalization, Flatten, GRU
import argparse 

parser = argparse.ArgumentParser()
parser.add_argument("--dataDirectory", type=str, default='/eos/user/a/agekow/tracking/data/trainingPkl/withLayerInfo/', help="file name of pickle data")
parser.add_argument("--epochs", type=int, default=50, help="number of epochs to train for")
parser.add_argument("--batch", type=int, default=128, help="batch size")
parser.add_argument("--modelType", type=str, required=True, help="MLP, RNN, ESN, qMLP")
parser.add_argument("--modelName", type=str, required=True, help="Name of saved model")
parser.add_argument("--bits", type=int, default=8, help="Number of bits if using quantized network")
parser.add_argument("--local", action='store_true', help="running locally or on lxplus")
args = parser.parse_args()

if args.local is True:
	gpu_devices = tf.config.experimental.list_physical_devices('GPU')
	for device in gpu_devices:
		tf.config.experimental.set_memory_growth(device, True)

X = None
Y = None
for data in os.listdir(args.dataDirectory): 
    with open(args.dataDirectory + data,'rb') as pFile:

        _ = pickle.load(pFile)
        _ = pickle.load(pFile)
        x_train_rnn = pickle.load(pFile, encoding='latin1')
        x_test_rnn = pickle.load(pFile,  encoding='latin1')
        x_train = pickle.load(pFile,  encoding='latin1')
        x_test = pickle.load(pFile,  encoding='latin1')
        x_train_layer = pickle.load(pFile,  encoding='latin1')
        x_test_layer = pickle.load(pFile,  encoding='latin1')
        _ = pickle.load(pFile,  encoding='latin1')
        _ =  pickle.load(pFile,  encoding='latin1')
        _ = pickle.load(pFile,  encoding='latin1')
        _ = pickle.load(pFile,  encoding='latin1')
        _ = pickle.load(pFile,  encoding='latin1')
        _ = pickle.load(pFile,  encoding='latin1')
        _ = pickle.load(pFile,  encoding='latin1')
        _ = pickle.load(pFile,  encoding='latin1')
        reg_train = pickle.load(pFile,  encoding='latin1')
        reg_test = pickle.load(pFile,  encoding='latin1')

    if X is None:
        X = x_train
        X = np.append(X, x_test, axis=0)
        Y = reg_train
        Y = np.append(Y, reg_test, axis=0)
    else:
        X = np.append(X, x_train, axis=0)
        X = np.append(X, x_test, axis=0)
        Y = np.append(Y, reg_train, axis=0)
        Y = np.append(Y, reg_test, axis=0)

print(X.shape, Y.shape)
x_train, x_test, y_train, y_test = train_test_split(X,Y, test_size = .2)

if args.modelType == 'MLP':
	model = paramEstimators.paramEstimator_mlp(nHits=x_train.shape[1])
else:
    raise Exception("Other model types not incorporated yet")

checkpoint_filepath = 'checkpoints/' + args.modelName #+ '/' + args.modelName
if not os.path.exists(checkpoint_filepath): os.mkdir(checkpoint_filepath)
callbacks = [tf.keras.callbacks.ModelCheckpoint(checkpoint_filepath, monitor='val_loss', verbose=0, save_best_only=True, save_weights_only=True),
			tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=10)]

# Include cyclical learning rates in the future
model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=1e-4), loss='mse')
history = model.fit(x_train, y_train, epochs=args.epochs, batch_size=args.batch, validation_data=(x_test, y_test), callbacks=callbacks)
model.load_weights(checkpoint_filepath)
#model.save('trained_models/' +args.modelName)
tf.keras.models.save_model(model, 'trained_models/' +args.modelName)
plt.plot(range(len(history.history['loss'])), history.history['loss'], label='train')
plt.plot(range(len(history.history['val_loss'])), history.history['val_loss'], label='train')
plt.xlabel('epochs')
plt.ylabel('loss')
plt.savefig('trained_models/{}_loss'.format(args.modelName))
